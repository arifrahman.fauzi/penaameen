@extends('layouts.app')
@section('header')
<h5 class="m-b-10">Dashboard</h5>
@endsection
@section('css')

@endsection
@section('header')
<h5>Data Mitra</h5>
@endsection
@section('breadcrumb')
<ul class="breadcrumb-title">
    <li class="breadcrumb-item">
        <a href="{{ url('/') }}"> <i class="fa fa-home"></i> </a>
    </li>
    <li class="breadcrumb-item"><a href="#!">Mitra</a>
    </li>
</ul>
@endsection
@section('content')
<div class="row">
    <div class="col-md-10">
      @if (session('success-update'))
          <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong> {{ session('success-update') }} </strong>
          </div>
      @endif
      @if (session('delete'))
          <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong> {{ session('delete') }} </strong>
          </div>
      @endif
        <div class="card">
            <div class="card-header">
                <h5>Data Mitra</h5>
            </div>
            <div class="card-block">
                <div class="table-responsive">
                    <table id="tbMitra" class="table table-hover">
                        <thead>
                            <tr>
                                <th>No</th>

                                <th>Nama Mitra</th>
                                <th>Alamat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1;
                            @endphp
                            @forelse ($data as $mitra)
                            <tr>
                                <td hidden>{{ $mitra->id_mitra }}</td>
                                <td>{{ $no++ }}</td>
                                <td hidden>{{ $mitra->id_mitra }}</td>
                                <td>{{ $mitra->nama_mitra }}</td>
                                <td>{{ $mitra->alamat }}</td>
                                <td>


                                  <button class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon btnEdit" onclick="location.href='{{ route('mitra.edit',$mitra->id_mitra) }}'"><i class="ti-pencil"></i></button>


                                    <button class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon btnEdit" onclick="location.href='{{ route('mitra.edit',$mitra->id_mitra) }}'"><i class="ti-pencil"></i></button>


                                  <form action="{{ route('mitra.destroy', $mitra->id_mitra) }}" method="post" style="display:inline-block">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon btnDelete"><i class="ti-trash"></i></button>
                                  </form>


                                  {{-- <a href="{{ route('mitra.destroy', $mitra->id_mitra) }}" data-method="delete" data-token="{{csrf_token()}}"></a> --}}

                                </td>
                            </tr>
                            @empty
                            <div class="alert alert-danger">
                                <strong>Data Masih Kosong !</strong>
                            </div>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

</div>

@endsection
@section('javascript')
  {{-- <script src="js/kelola-mitra.js" charset="utf-8"></script> --}}
<script>
// $(document).ready(function() {
//   console.log('kelola mitra ready');
// });
//
// $('#tbMitra').on('click', '.btnEdit', function(event) {
//   event.preventDefault();
//   var id = $(this).closest('tr').find('td:eq(0)').text();
//   console.log(id);
//   /* Act on the event */
//   $('.edit').attr('readonly',false);
//   $(this).attr('hidden',true);
//   $('#btnSave').attr('hidden',false);
// });
//
// $('.btnSave').click(function(event) {
//   $('.edit').attr('readonly',true);
//     $('#btnEdit').attr('hidden',false);
// });

</script>
@endsection
