@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              <h5>Kelola User</h5>
              <button class="btn btn-primary btn-round waves-effect waves-light add" style="position:absolute;right:10px;top:15px">Tambah</button>
            </div>
            <div class="card-block">
              <div class="table-responsive">
                <table id="tbUser" class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Role</th>
                      <th>Id GMF</th>
                      <th>Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $no = 1; @endphp
                    @forelse ($array as $data)
                      <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->username }}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->nama_role }}</td>
                        <td>{{ $data->id_mitra }}</td>
                        <td>
                          <button class="btn waves-effect waves-light btn-warning edit" value="{{ $data->id }}"><i class="fa fa-pencil"></i>Edit</button>
                          <form action="{{ route('user.destroy',$data->id) }}" method="post" style="display:inline-block">
                            @csrf @method('DELETE')
                            <button type="submit" name="button" class="btn waves-effect waves-light btn-danger">Delete</button>
                          </form>
                        </td>
                      </tr>
                    @empty
                      <p>Data Masih Kosong</p>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
        </div>
<!--Modal: ADD-->
<div class="modal fade" id="modalPush" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content text-center">
      <!--Header-->
      <div class="modal-header d-flex justify-content-center">
        <h5>Tambah User</h5>
      </div>

      <!--Body-->
      <div class="modal-body">
      <form class="" action="{{ route('user.store') }}" method="post">
        @csrf
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Nama Terang</label>
          <div class="col-sm-6">
              <input type="text" name="name" class="form-control">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Id GMF</label>
          <div class="col-sm-6">
              <input type="text" name="id_gmf" class="form-control" required>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Username</label>
          <div class="col-sm-6">
              <input type="text" name="username" class="form-control" required>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Role</label>
          <div class="col-sm-4">
              <select class="custom-select" name="role">
                @foreach ($role as $roles)
                  <option value="{{ $roles->id_role }}">{{ $roles->nama_role }}</option>
                @endforeach
              </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Email</label>
          <div class="col-sm-6">
              <input type="email" name="email" class="form-control">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Password</label>
          <div class="col-sm-6">
              <input type="password" name="password" class="form-control">
          </div>
        </div>
        <button type="submit" name="button" class="btn btn-info">Create</button>
        <button type="button" name="button" data-dismiss="modal" class="btn btn-outline-info waves-effect">Close</button>
      </form>
      </div>

      <!--Footer-->
      <div class="modal-footer flex-center">
        {{-- <a href="https://mdbootstrap.com/products/jquery-ui-kit/" class="btn btn-info">Yes</a> --}}
        {{-- <a type="button" class="btn btn-outline-info waves-effect" data-dismiss="modal">close</a> --}}
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!--Modal: modalAdd-->
<!--Modal: modalEdit-->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content text-center">
      <!--Header-->
      <div class="modal-header d-flex justify-content-center">
        <h5>Tambah User</h5>
      </div>

      <!--Body-->
      <div class="modal-body">
      <form id="formEdit" method="post">
        @method('PUT')
        @csrf
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Nama Terang</label>
          <div class="col-sm-6">
              <input id="nama" type="text" name="name" class="form-control">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Id GMF</label>
          <div class="col-sm-6">
              <input id="idGmf" type="text" name="id_gmf" class="form-control">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Username</label>
          <div class="col-sm-6">
              <input id="username" type="text" name="username" class="form-control">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Role</label>
          <div class="col-sm-4">
              <select class="custom-select" name="role">
                @foreach ($role as $roles)
                  <option value="{{ $roles->id_role }}">{{ $roles->nama_role }}</option>
                @endforeach
              </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Email</label>
          <div class="col-sm-6">
              <input id="email" type="email" name="email" class="form-control">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-3 col-form-label">Password</label>
          <div class="col-sm-6">
              <input type="password" name="password" class="form-control">
          </div>
        </div>
        <button type="submit" name="button" class="btn btn-primary">Edit</button>
        <button type="button" name="button" data-dismiss="modal" class="btn btn-outline-info waves-effect">Close</button>
      </form>
      </div>

      <!--Footer-->
      <div class="modal-footer flex-center">
        {{-- <a href="https://mdbootstrap.com/products/jquery-ui-kit/" class="btn btn-info">Yes</a> --}}
        {{-- <a type="button" class="btn btn-outline-info waves-effect" data-dismiss="modal">close</a> --}}
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!--Modal: modalEdit-->
    </div>
</div>
@endsection

@section('javascript')
<script>
$(document).ready(function() {
  $('.add').click(function(event) {
    $('#modalPush').modal('show');
  });
$('#tbUser').on('click', '.edit', function(event) {
  event.preventDefault();
  var id = $(this).val();
  var nama = $(this).closest('tr').find('td:eq(1)').text();
  var username = $(this).closest('tr').find('td:eq(2)').text();
  var email = $(this).closest('tr').find('td:eq(3)').text();
  var idgmf = $(this).closest('tr').find('td:eq(5)').text();
  $('#modalEdit').modal('show');
  $('#nama').val(nama);
  $('#username').val(username);
  $('#idGmf').val(idgmf);
  $('#email').val(email);
  $('#formEdit').attr('action','{{ url('user') }}'+'/'+id);

});

});
</script>
@endsection
