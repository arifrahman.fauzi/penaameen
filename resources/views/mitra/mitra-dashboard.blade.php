@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4>Biodata Mitra</h4>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xl-4">
                        <img src="{{ url('storage/gmfphotos/'.$data->photo) }}" class="" alt="User-Profile-Image" style="width:240px;height:240px;">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xl-8">
                      <div class="col-md-12 col-sm-12 col-xl-12">
                        <ul class="nav nav-tabs md-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#datadiri" role="tab">Data Diri</a>
                                <div class="slide"></div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#sanggar" role="tab">Lembaga</a>
                                <div class="slide"></div>
                            </li>
                        </ul>
                      </div>
                        {{-- Tab Panes --}}
                        <div class="col-md-12 col-sm-12 col-xl-12">
                        <div class="tab-content card-block">
                            <div class="tab-pane active" id="datadiri" role="tabpanel">
                                <div class="form-group row">
                                    <label class="col-sm-3">Nama</label>
                                    <div class="col-sm-5">
                                        <div class="form-control-static">{{ $data->nama_mitra }}
                                        </div> <span>{{ $data->jenis_kelamin }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">NIK</label>
                                    <div class="col-sm-5">
                                        <div class="form-control-static">{{ $data->nik }}</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">Tempat tgl lahir</label>
                                    <div class="col-sm-5">
                                        <div class="form-control-static">{{ $data->kota_lahir }} {{ $data->tgl_lahir }}</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">Nomor Anggota</label>
                                    <div class="col-sm-5">
                                        <div class="form-control-static">{{ $data->nomor_anggota }}</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3">Nomor Telp</label>
                                    <div class="col-sm-5">
                                        <div class="form-control-static">+62 {{ $data->nomor_telp }}</div>
                                    </div>


                                </div>
                            </div>
                            <div class="tab-pane" id="sanggar" role="tabpanel">
                              <div class="form-group row">
                                <label class="col-sm-3">Nama Lembaga</label>
                                <div class="col-sm-5">
                                  <div class="form-control-static">
                                    {{ $data->nama_lembaga }}
                                  </div>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3">Telp Lembaga</label>
                                <div class="col-sm-5">
                                  <div class="form-control-static">
                                    +62 {{ $data->tlp_lembaga }}
                                  </div>
                                </div>
                              </div>

                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
