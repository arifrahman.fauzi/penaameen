@extends('layouts.app')
@section('css')
  <!-- Notification.css -->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/notification/notification.css')}}">
      <!-- Animate.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css/css/animate.css')}}">
@endsection
@section('header')
<h5 class="m-b-10">Profile</h5>
<p class="m-b-0">user profile</p>
@endsection
@section('breadcrumb')
<ul class="breadcrumb-title">
    <li class="breadcrumb-item">
        <a href="index.html"> <i class="ti-user"></i> </a>
    </li>
    <li class="breadcrumb-item"><a href="#!">Profile</a>
    </li>
</ul>
@endsection
@section('content')
<div class="row">
  {{-- card 1 --}}
    <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            <h5>Profil User</h5>
            <button id="btnSave" class="btn waves-effect waves-light btn-success btn-icon" style="position:absolute;right:10px;top:15px"><i class="ti-pencil"></i></button>
            <button id="btnEdit" class="btn waves-effect waves-light btn-warning btn-icon" style="position:absolute;right:10px;top:15px"><i class="ti-pencil"></i></button>

          </div>
          <div class="card-block">
            <form id="formProfile" class="" action="{{ route('profile.update',auth::user()->id) }}" method="post">
              @csrf
              @method('PUT')
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                  <input type="text" name="nama" class="edit form-control" value="{{ auth::user()->name }}" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10">
                  <input type="text" name="username" class="edit form-control" value="{{ auth::user()->username }}" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" name="email" class="edit form-control" value="{{ auth::user()->email }}" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Role</label>
                <div class="col-sm-10">
                  <input type="text" name="role" class="form-control" value="{{ $user->nama_role }}" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Created at</label>
                <div class="col-sm-10">
                  <input type="text" name="created at" class="form-control" value="{{ $user->created_at }}" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Updated at</label>
                <div class="col-sm-10">
                  <input type="text" name="role" class="form-control" value="{{ $user->updated_at }}" readonly>
                </div>
              </div>
            </form>

          </div>
        </div>
    </div>
    {{-- end card 1 --}}
    {{-- card 2 --}}
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">

        </div>
        <div class="card-block">
          <div class="col-md-12">

            <img src="storage/gmfphotos/{{ $user->photo }}" alt="User-Profile-Image" class="img-120" style="width:100%">
            <form action="/profile/avatar" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                      <input type="file" class="form-control-file" name="avatar" id="avatarFile" aria-describedby="fileHelp">
                      <small id="fileHelp" class="form-text text-muted"> ukuran 512 x 512 di bawah 2 mb</small>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>

        </div>

      </div>

    </div>
    {{-- end card 2 --}}
</div>
<div class="row">
<div class="col-md-12 col-sm-12 col-xl-12">
<div class="card">
<div class="card-header">
<h3>Biodata Diri</h3>
<button id="edit" class="btn waves-effect waves-light btn-info" style="position:absolute;right:10px;top:15px" ><i class="ti-pencil"></i>Edit</button>
<button id="save" class="btn waves-effect waves-light btn-primary" style="position:absolute;right:10px;top:15px" hidden><i class="ti-save"></i>Save</button>
</div>
<div class="card-block">
<form id="formdata" action="{{ url('profile/'.$profil->id_mitra)}}" method="post">
  @csrf
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Nama Terang</label>
    <div class="col-sm-10">
      <input type="text" name="nama_terang" class="ubah form-control" value="{{ $profil->nama_mitra }}" readonly>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">NIK</label>
    <div class="col-sm-10">
      <input type="text" name="nik" class="ubah form-control" value="{{ $profil->nik }}" readonly>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Nomor Anggota</label>
    <div class="col-sm-10">
      <input type="text" name="nomor_anggota" class="ubah form-control" value="{{ $profil->nomor_anggota }}" readonly @if (checkPermission(['gmf','personal','cabang']))
        disabled
      @endif>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Nomor Sanggar</label>
    <div class="col-sm-10">
      <input type="text" name="nomor_sanggar" class="ubah form-control" value="{{ $profil->nomor_sanggar }}" readonly @if (checkPermission(['gmf','personal','cabang']))
        disabled
      @endif>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Nomor Sertifikat</label>
    <div class="col-sm-10">
      <input type="text" name="nomor_sertifikat" class="ubah form-control" value="{{ $profil->nomor_sertifikat }}" readonly @if (checkPermission(['gmf','personal','cabang']))
        disabled
      @endif>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Nomor Lisensi</label>
    <div class="col-sm-10">
      <input type="text" name="nomor_lisensi" class="ubah form-control" value="{{ $profil->nomor_lisensi }}" readonly @if (checkPermission(['gmf','personal','cabang']))
        disabled
      @endif>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Nomor Telp</label>
    <div class="col-sm-10">
      <input type="text" name="nomor_telp" class="ubah form-control" value="{{ $profil->nomor_telp }}" readonly>
    </div>
  </div>
  @php
    $kotalahir = DB::table('kabupaten')->select('Nama')->where('IDKabupaten', $profil->kota_lahir)->first();
  @endphp
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Kota Lahir</label>
    <div class="col-sm-10">
      <select class="chosen custom-selct" name="kota_lahir">

        @foreach ($kota as $kotas)
          <option value="{{ $kotas->IDKabupaten }}" @if ($profil->kota_lahir == $kotas->IDKabupaten)
            selected
          @endif>{{ $kotas->Nama }}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Tanggal Lahir</label>
    <div class="col-sm-10">
      <input type="text" name="kota_lahir" class="ubah form-control" value="{{ $profil->tgl_lahir }}" readonly>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Alamat</label>
    <div class="col-sm-10">
      <input type="text" name="alamat" class="ubah form-control" value="{{ $profil->alamat }}" readonly>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Status Kawin</label>
    <div class="col-sm-10">
      <input type="text" name="status_kawin" class="ubah form-control" value="{{ $profil->status_kawin }}" readonly>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Warga Negara</label>
    <div class="col-sm-10">
      <input type="text" name="warga_negara" class="ubah form-control" value="{{ $profil->warga_negara }}" readonly>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Agama</label>
    <div class="col-sm-10">
      <input type="text" name="agama" class="ubah form-control" value="{{ $profil->agama }}" readonly>
    </div>
  </div>
</form>
</div>
</div>
</div>
</div>
@endsection
@section('javascript')
  <!-- notification js -->
  <script type="text/javascript" src="{{asset('assets/js/bootstrap-growl.min.js')}}"></script>
  {{-- <script type="text/javascript" src="{{asset('assets/pages/notification/notification.js')}}"></script> --}}
  {{-- <script src="{{asset('assets/js/pcoded.min.js')}}"></script> --}}
  {{-- <script src="{{asset('assets/js/vertical-layout.min.js')}} "></script>
  <script src="{{asset('assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script> --}}
  <script src="{{ asset('assets/js/custom-notif.js') }}"></script>
<script src="{{asset('js/edit-profile.js')}}"></script>
<script>
  $('.chosen').chosen();
</script>
@endsection
