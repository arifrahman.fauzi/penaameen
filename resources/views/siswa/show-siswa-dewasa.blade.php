@extends('layouts.app')
@section('css')

@endsection
@section('header')
<h5>Data Siswa</h5>
@endsection
@section('breadcrumb')
<ul class="breadcrumb-title">
    <li class="breadcrumb-item">
        <a href="{{ url('/') }}"> <i class="fa fa-home"></i> </a>
    </li>
    <li class="breadcrumb-item"><a href="#!">Data Siswa</a>
    </li>
</ul>
@endsection
@section('content')
<div class="row">

    <div class="col-md-12">
      <div class="card">
          <div class="card-header">
              <h5>Lihat Data Siswa</h5>
          </div>
          <div class="card-block">
            @forelse ($data as $nilai)

                <form class="" action="{{ url('siswa_dewasa',$nilai->id_siswa) }}" method="post">
                  @csrf
                  @method('PUT')

                  <div class="form-group row">
                    <label  class="col-sm-2 col-form-label">Nama Siswa</label>
                    <div class="col-sm-10">
                      <input name="nama" type="text" class="form-control" value="{{$nilai->nama_siswa}}" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                      <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1" disabled>
                        <option value="laki - laki" @if($nilai->jenis_kelamin=='laki - laki')selected @endif>Laki Laki</option>
                        <option value="perempuan" @if($nilai->jenis_kelamin=='perempuan')selected @endif>Perempuan</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">tempat lahir</label>
                      <div class="col-sm-10">
                    <input name="tempat_lahir" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="tempat lahir" value="{{$nilai->tempat_lahir}}" readonly>
                  </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">tanggal lahir</label>
                    <div class="col-sm-10">
                    <input name="tanggal_lahir" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="tanggal lahir" value="{{$nilai->tanggal_lahir}}" readonly>
                  </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Umur</label>
                    <div class="col-sm-10">
                    <input name="umur" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="umur" value="{{$nilai->umur}}" readonly>
                  </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Alamat Rumah</label>
                    <div class="col-sm-10">
                      <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{$nilai->alamat}}</textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pendidikan</label>
                    <div class="col-sm-10">
                      <input name="pendidikan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{$nilai->pendidikan}}" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pekerjaan</label>
                    <div class="col-sm-10">
                      <input name="pekerjaan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{$nilai->pekerjaan}}" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">No_telepon</label>
                    <div class="col-sm-10">
                      <input name="no_telp" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{$nilai->no_telepon}}" readonly>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Agama</label>
                    <div class="col-sm-10">
                      <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{$nilai->agama}}" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Suku</label>
                    <div class="col-sm-10">
                      <input name="suku" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{$nilai->suku}}" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Riwayat penyakit</label>
                    <div class="col-sm-10">
                    <input name="riwayat_penyakit" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{($nilai->riwayat_penyakit)}}" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Program yang dipilih</label>
                    <div class="col-sm-10">
                      <select name="program_kursus" class="form-control" id="exampleFormControlSelect1" disabled>
                      <option value="AL BARQY DEWASA" @if($nilai->program_kursus=='AL BARQY DEWASA')selected @endif>Al-barqy</option>
                    </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">hari kursus</label>
                    <div class="col-sm-10">
                      <input name="hari_kursus" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{$nilai->hari_kursus}}" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">waktu kursus</label>
                    <div class="col-sm-10">
                      <input name="waktu_kursus" type="time" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{$nilai->waktu_kursus}}" readonly>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jenis kelas</label>
                    <div class="col-sm-10">
                      <select name="jenis_kelas" class="form-control" id="exampleFormControlSelect1" disabled>
                      <option  value="PRIVAT" @if($nilai->jenis_kelas=='PRIVATE')selected @endif>PRIVAT</option>
                      <option value="REGULER"  @if($nilai->jenis_kelas=='REGULER')selected @endif>REGULER</option>
                      <option value="KHUSUS"  @if($nilai->jenis_kelas=='KHUSUS')selected @endif>KHUSUS</option>
                    </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Upload foto siswa</label>
                    <div class="col-sm-10">
                      <input type="file" class="form-control-file" id="exampleFormControlFile1" disabled>
                      </div>

                    </div>
                <button type="" class="btn btn-primary" name"button">back</button>
                </form>
                @empty
                  <div class="alert alert-danger">
                      <strong>Data Masih Kosong !</strong>
                  </div>
                @endforelse

          </div>
      </div>
    </div>
</div>
@endsection
