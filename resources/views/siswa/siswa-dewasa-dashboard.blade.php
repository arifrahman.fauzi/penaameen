@extends('layouts.app')
@section('css')

@endsection
@section('header')
<h5>Data Siswa</h5>
@endsection
@section('breadcrumb')
<ul class="breadcrumb-title">
    <li class="breadcrumb-item">
        <a href="{{ url('/') }}"> <i class="fa fa-home"></i> </a>
    </li>
    <li class="breadcrumb-item"><a href="#!">Data Siswa</a>
    </li>
</ul>
@endsection
@section('content')
<div class="row">
      <div class="col-6">
              <!-- button -->


      </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Data Siswa Dewasa</h5>

            </div>
            <div class="card-block">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Siswa</th>
                            <th>Tempat & Tanggal lahir</th>
                            <th>alamat</th>
                            <th>program yang dipilih</th>
                            <th>hari kursus</th>
                            <th>kelas kursus</th>
                            <th>Aksi</th>
                          </tr>

                        </thead>
                        <tbody>
                          @php $no=1; @endphp
                          @forelse ($data as $siswa)
                            <tr>
                              <td>{{ $no++ }}</td>
                              <td hidden>{{ $siswa->id_siswa }}</td>
                              <td>{{ $siswa->nama_siswa }}</td>
                              <td hidden>{{ $siswa->jenis_kelamin}}</td>
                              <td>{{ $siswa->tempat_lahir}} ,{{ $siswa->tanggal_lahir}}</td>
                              <td>{{ $siswa->alamat }}</td>
                              <td>{{ $siswa->program_kursus }}</td>
                              <td>{{ $siswa->hari_kursus }}</td>
                              <td>{{ $siswa->jenis_kelas }}</td>
                              <td>
                                <a href="/siswa_dewasa/{{$siswa->id_siswa}}" class="btn btn-success btn-sm">show</a>
                                <a href="/siswa_dewasa/{{$siswa->id_siswa}}/edit" class="btn btn-warning btn-sm">Edit</a>

                                <form action="{{ url('siswa_dewasa', $siswa->id_siswa) }}" method="post" style="display:inline-block">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn waves-effect waves-dark btn-danger btn-sm" onclick="return confirm('apakah yakin menghapus data?')">Hapus</button>
                                </form>

                              </td>
                            </tr>
                          @empty
                            <div class="alert alert-danger">
                                <strong>Data Masih Kosong !</strong>
                            </div>
                          @endforelse


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
