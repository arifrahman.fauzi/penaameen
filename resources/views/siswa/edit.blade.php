@extends('layouts.app')
@section('css')

@endsection
@section('header')
<h5>Data Siswa</h5>
@endsection
@section('breadcrumb')
<ul class="breadcrumb-title">
    <li class="breadcrumb-item">
        <a href="{{ url('/') }}"> <i class="fa fa-home"></i> </a>
    </li>
    <li class="breadcrumb-item"><a href="#!">Data Siswa</a>
    </li>
</ul>
@endsection
@section('content')
<div class="row">

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Edit Data Siswa</h5>
            </div>
            <div class="card-block">
              @forelse ($data as $nilai)

                  <form class="" action="{{ route('siswa.update',$nilai->id_siswa) }}" method="post">
                    @csrf
                    @method('PUT')

                    <div class="form-group row">
                      <label  class="col-sm-2 col-form-label">Nama Siswa</label>
                      <div class="col-sm-10">
                        <input name="nama" type="text" class="form-control" value="{{$nilai->nama_siswa}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                      <div class="col-sm-10">
                        <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
                          <option value="laki - laki" @if($nilai->jenis_kelamin=='laki - laki')selected @endif>Laki Laki</option>
                          <option value="perempuan" @if($nilai->jenis_kelamin=='perempuan')selected @endif>Perempuan</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Ortu</label>
                        <div class="col-sm-10">
                          <input name="nama_ortu" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="ayah" value="{{$nilai->nama_ortu}}">
                        </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">tempat lahir</label>
                        <div class="col-sm-10">
                      <input name="tempat_lahir" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="tempat lahir" value="{{$nilai->tempat_lahir}}">
                    </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">tanggal lahir</label>
                      <div class="col-sm-10">
                      <input name="tanggal_lahir" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="tanggal lahir" value="{{$nilai->tanggal_lahir}}">
                    </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Umur</label>
                      <div class="col-sm-10">
                      <input name="umur" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="umur" value="{{$nilai->umur}}">
                    </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Anak ke</label>
                      <div class="col-sm-10">
                      <input name="anak_ke" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="anak ke" value="{{$nilai->anak_ke}}">
                    </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Berapa Bersaudara</label>
                      <div class="col-sm-10">
                        <input name="bersaudara" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{$nilai->bersaudara}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Alamat Rumah</label>
                      <div class="col-sm-10">
                        <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$nilai->alamat}}</textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Sekolah</label>
                      <div class="col-sm-10">
                      <input name="nama_sekolah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Sekolah" value="{{$nilai->nama_sekolah}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Kelas</label>
                      <div class="col-sm-10">
                        <input name="kelas" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{$nilai-> kelas}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Alamat Sekolah</label>
                      <div class="col-sm-10">
                      <textarea name="alamat_sekolah" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$nilai->alamat_sekolah}}</textarea>
                    </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Agama</label>
                      <div class="col-sm-10">
                        <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{$nilai->agama}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Suku</label>
                      <div class="col-sm-10">
                        <input name="suku" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{$nilai->suku}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Bimbel Tambahan</label>
                      <div class="col-sm-10">
                      <input name="bimbel" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{$nilai->bimbel}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Dengan Siapa Sepulang Sekolah</label>
                      <div class="col-sm-10">
                      <input name="teman_dirumah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" value="{{$nilai->teman_dirumah}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Guru Dirumah</label>
                      <div class="col-sm-10">
                      <input name="guru_dirumah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{$nilai->guru_dirumah}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Apakah sepulang sekolah tidur siang</label>
                      <div class="form-check">
                        <div class="col-sm-10">
                        <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                        <label class="form-check-label" for="gridRadios1">
                          Iya
                        </label>
                      </div>
                      </div>
                      <div class="form-check">
                        <div class="col-sm-10">
                        <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                        <label class="form-check-label" for="gridRadios2">
                          Tidak
                        </label>
                      </div>
                    </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Bagaimana minat belajar dirumah</label>
                      <div class="col-sm-10">
                        <input name="minat_belajar_dirumah" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{$nilai->minat_belajar_dirumah}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Mainan kesukaan</label>
                      <div class="col-sm-10">
                      <input name="mainan_kesukaan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{$nilai->mainan_kesukaan}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Apakah sudah mengenal gadget</label>
                      <div class="col-sm-10">
                        <input name="mengenal_gadget" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{$nilai->mengenal_gadget}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">sering bermain gadget</label>
                      <div class="col-sm-10">
                        <input name="bermain_gadget" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{$nilai->sering_bermain_gadget}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Riwayat penyakit</label>
                      <div class="col-sm-10">
                      <input name="riwayat_penyakit" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{($nilai->riwayat_penyakit)}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Program yang dipilih</label>
                      <div class="col-sm-10">
                        <select name="program_kursus" class="form-control" id="exampleFormControlSelect1">
                        <option value="ALBARQY" @if($nilai->program_kursus=='ALBARQY')selected @endif>Al-barqy</option>
                        <option value="ACM" @if($nilai->program_kursus=='ACM')selected @endif>Aku Cepat Membaca(ACM)</option>
                        <option value="JALPIN" @if($nilai->program_kursus=='JALPIN')selected @endif>Jalpin</option>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">hari kursus</label>
                      <div class="col-sm-10">
                        <input name="hari_kursus" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{$nilai->hari_kursus}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">waktu kursus</label>
                      <div class="col-sm-10">
                        <input name="waktu_kursus" type="time" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="kelas" value="{{$nilai->waktu_kursus}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Jenis kelas</label>
                      <div class="col-sm-10">
                        <select name="jenis_kelas" class="form-control" id="exampleFormControlSelect1">
                        <option  value="PRIVATE" @if($nilai->jenis_kelas=='PRIVATE')selected @endif>PRIVATE</option>
                        <option value="REGULER"  @if($nilai->jenis_kelas=='REGULER')selected @endif>REGULER</option>
                        <option value="KHUSUS"  @if($nilai->jenis_kelas=='KHUSUS')selected @endif>KHUSUS</option>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Upload foto siswa</label>
                      <div class="col-sm-10">
                        <input type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>
                      </div>

                  <button type="submit" class="btn btn-primary" name"button">Save</button>
                  </form>
                  @empty
                    <div class="alert alert-danger">
                        <strong>Data Masih Kosong !</strong>
                    </div>
                  @endforelse

            </div>
        </div>
    </div>
</div>
@endsection
