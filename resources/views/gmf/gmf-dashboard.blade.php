@extends('layouts.app')
@section('css')
<style>
.modal-dialog {
  max-width: 600px;
  margin: 60px auto;
}
</style>
@endsection
@section('content')
  <!--Modal: Alert-->
  <div class="modal fade" id="modalPush" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-animation-in="animated fadeIn" data-animation-out="animated fadeOut">
      <div class="modal-dialog modal-notify modal-info" role="document">
          <!--Content-->
          <div class="modal-content text-center">
              <!--Header-->
              <div class="modal-header d-flex justify-content-center">
                  <h5>Isi Biodata</h5>
              </div>
              <!--Body-->
              <div class="modal-body">

              </div>

              <!--Footer-->
              <div class="modal-footer flex-center">
                  <a href="https://mdbootstrap.com/products/jquery-ui-kit/" class="btn btn-info">Yes</a>
                  <a type="button" class="btn btn-outline-info waves-effect" data-dismiss="modal">No</a>
              </div>
          </div>
          <!--/.Content-->
      </div>
  </div>
  <!--Modal: modalPush-->
<div class="row">
    <div class="col-md-12">
      @if (session('delete'))
          <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong> {{ session('delete') }} </strong>
          </div>
      @endif
      @if (session('update'))
          <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong> {{ session('update') }} </strong>
          </div>
      @endif
        <div class="card">
            <div class="card-header">
                <h5>Daftar GMF</h5>
                <button class="btn btn-primary btn-round waves-effect waves-light" style="position:absolute;right:10px;top:15px" onclick="location.href='{{ route('gmf.create') }}'">Tambah</button>

            </div>
            <div class="card-block">


              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nomor GMF</th>
                      <th>Nama</th>
                      <th>Alamat</th>
                      <th>Nomor Tlp</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $no = 1; @endphp
                    @forelse ($gmf as $data)
                      <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $data->nomor_gmf }}</td>
                        <td>{{ $data->nama }}</td>
                        <td>{{ $data->alamat }}</td>
                        <td>{{ $data->nomer_tlp }}</td>
                        <td>
                          <button class="btn waves-effect waves-light btn-warning edit" onclick="location.href='{{ route('gmf.edit',$data->nik) }}'">Edit</button>
                          <form class="" action="{{ route('gmf.destroy',$data->nik) }}" method="post" style="Display:inline-block">
                            @csrf
                            @method('DELETE')
                            <button class="btn waves-effect waves-light btn-danger" type="submit">Delete</button>
                          </form>
                          <button class="btn waves-effect waves-light btn-success" onclick="location.href='{{ route('gmf.show',$data->nik) }}'">Lihat</button>
                        </td>
                      </tr>
                    @empty
                      <p>Data Masih Kosong</p>
                    @endforelse
                  </tbody>
                </table>
              </div>

            </div>
        </div>
    </div>
    <div class="col-md-2">
      <div class="card">

      </div>
    </div>
</div>
@endsection
@section('javascript')

@endsection
