@extends('layouts.app')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-sl-12">
      @if (session('success'))
          <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong> {{ session('success') }} </strong>
          </div>
      @endif

    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
              <h5>Input Data</h5>
            </div>
            <div class="card-block">
              <form class="" action="{{ route('gmf.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama Terang</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">NIK</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Tempat Tgl Lahir</label>
                    <div class="col-sm-10">
                      <select id="provinsi" class="custom-select" name="provinsi">
                        <options selected>pilih ..</option>
                        @foreach ($wilayah as $kota)
                          <option value="{{ $kota->IDProvinsi }}">{{ $kota->Nama }}</option>
                        @endforeach
                      </select>
                      <select id="kota" class="custom-select" name="kota"></select>
                      <select id="kecamatan" class="custom-select" name="kecamatan"></select>
                      <select id="kelurahan" class="custom-select" name="kelurahan"></select>
                        {{-- <select class="form-control" name="kota">
                          @foreach ($wilayah as $kota)
                            <option value="{{ $kota->kode }}">{{ $kota->nama }}</option>
                          @endforeach
                        </select> --}}
                    </div>

                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status Perkawinan</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status Warga Negara</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Agama</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Suku</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jumlah Anak Laki -Laki</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jumlah Anak Perempuan</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nomer Telp</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pendidikan</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pekerjaan</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">NIP</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">NPWP</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status Kepegawaian</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lembaga/Instansi</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">No.Telp Lembaga</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Alamat Lembaga</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jabatan</label>
                    <div class="col-sm-10">
                        <input type="text" name="" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Deskripsi</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" required=""></textarea>
                    <label class="col-sm-3 col-form-label">Nama Terang</label>
                    <div class="col-sm-5">
                        <input type="text" name="nama" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">NIK</label>
                    <div class="col-sm-5">
                        <input type="text" name="nik" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-4 col-from-label">Upload Foto</label>
                  <div class="col-sm-5">
                      <input type="file" class="form-control-file" name="photo" id="avatarFile" aria-describedby="fileHelp">
                      <small id="fileHelp" class="form-text text-muted">resolisi di atas 400 x 400. ukuran max 2MB.</small>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                  <div class="col-sm-5">
                    <select class="custom-select" name="jenis_kelamin">
                      <option value="laki-laki">laki - laki</option>
                      <option value="perempuan">perempuan</option>
                    </select>
                  </div>
                </div>
                {{-- <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Tempat Tinggal/Alamat Surat</label>
                  <div class="col-sm-4">
                    <textarea type="text" name="alamat" class="form-control" placeholder="Jl.xxxxx"></textarea>
                  </div>
                  <div class="col-sm-1 col-md-1 col-xl-1">
                    <input type="text" name="rw" placeholder="RW" class="form-control">
                  </div>
                  <div class="col-sm-1 col-md-1 col-xl-1">
                    <input type="text" name="rt" placeholder="RT" class="form-control">
                  </div>
                  <div class="col-sm- col-md-1 col-xl-1">
                    <input type="text" name="usia" placeholder="Usia" class="form-control">
                  </div>
                </div> --}}
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Tempat Tgl Lahir</label>
                  <div class="col-sm-4">
                    <select id="kotalahir" class="chosen custom-select" name="kotalahir">
                      @foreach ($kota as $tempat)
                        <option value="{{ $tempat->IDKabupaten }}">{{ $tempat->Nama }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <input type="date" name="tgl_lahir" value="" class="form-control">
                  </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Tempat Tinggal/Alamat Surat</label>

                      <div class="col-sm-4">
                        <textarea type="text" name="alamat" class="form-control" placeholder="Jl.xxxxx"></textarea>
                      </div>
                      <div class="col-sm-1">
                        <input type="text" name="rw" placeholder="RW" class="form-control">
                      </div>
                      <div class="col-sm-1">
                        <input type="text" name="rt" placeholder="RT" class="form-control">
                      </div>
                      <div class="col-sm-1">
                        <input type="text" name="usia" placeholder="Usia" class="form-control">
                      </div>


                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label"></label>
                      <div class="col-sm-9">
                        <select id="provinsi" class="custom-select" name="provinsi">
                          <options selected>pilih ..</option>
                          @foreach ($wilayah as $kota)
                            <option value="{{ $kota->IDProvinsi }}">{{ $kota->Nama }}</option>
                          @endforeach
                        </select>
                        <select id="kota" class="custom-select" name="kota">
                          <option> --------Select--------- </option>
                        </select>
                        <select id="kecamatan" class="custom-select" name="kecamatan">
                          <option> --------Select--------- </option>
                        </select>
                        <select id="kelurahan" class="custom-select" name="kelurahan">
                          <option> --------Select--------- </option>
                        </select>
                      </div>
                    </div>

                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Status Perkawinan</label>
                    <div class="col-sm-5">
                        <select class="custom-select" name="status_kawin">
                          <option value="sudah">sudah</option>
                          <option value="belum">belum</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Status Warga Negara</label>
                    <div class="col-sm-5">
                        <input type="text" name="status_warga" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Agama</label>
                    <div class="col-sm-5">
                        <select class="custom-select" name="agama">
                          <option value="islam">islam</option>
                          <option value="kristen">kristen</option>
                          <option value="khatolik">khatolik</option>
                          <option value="protestan">protenstan</option>
                          <option value="hindu">hindu</option>
                          <option value="budha">budha</option>
                          <option value="konghucu">konghucu</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Suku</label>
                    <div class="col-sm-5">
                        <input type="text" name="suku" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Jumlah Anak Laki -Laki</label>
                    <div class="col-sm-5">
                        <input type="text" name="jumlah_l" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Jumlah Anak Perempuan</label>
                    <div class="col-sm-5">
                        <input type="text" name="jumlah_p" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Nomer Telp</label>
                    <div class="col-sm-5">
                        <input type="text" name="nomor" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-5">
                        <input type="email" name="email" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Pendidikan</label>
                    <div class="col-sm-5">
                      <select id="pendidikan" class="custom-select" name="pendidikan">
                            <option value="sarjana">Sarjana</option>
                            <option value="tidak tamat sd">Tidak tamat sd</option>
                            <option value="sd">sd</option>
                            <option value="smp">smp</option>
                            <option value="sma">sma</option>
                            <option value="lain">lain-lain</option>
                          </select>
                          <input id="other" type="text" name="other" class="form-control" style="width:50%" hidden>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Pekerjaan</label>
                    <div class="col-sm-5">
                        <input type="text" name="pekerjaan" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">NIP</label>
                    <div class="col-sm-5">
                        <input type="text" name="nip" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">NPWP</label>
                    <div class="col-sm-5">
                        <input type="text" name="npwp" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Status Kepegawaian</label>
                    <div class="col-sm-5">
                        <input type="text" name="status_pegawai" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Lembaga/Instansi</label>
                    <div class="col-sm-5">
                        <input type="text" name="lembaga" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">No.Telp Lembaga</label>
                    <div class="col-sm-5">
                        <input type="text" name="telp_lembaga" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Alamat Lembaga</label>
                    <div class="col-sm-5">
                        <input type="text" name="alamat_lembaga" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Jabatan</label>
                    <div class="col-sm-5">
                        <input type="text" name="jabatan" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Deskripsi</label>
                    <div class="col-sm-5">
                        <textarea class="form-control" name="deskripsi"></textarea>

                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-round waves-effect waves-light">Save</button>
              </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
<script>
$(document).ready(function() {
//$('.datepicker').datepicker();

$('#kotalahir').chosen();

  $('#pendidikan').change(function() {
    if ($(this).val() == 'lain') {
      $('#other').removeAttr('hidden');
    }else {
      $('#other').attr('hidden',true);
    }
  });


  $('#provinsi').change(function(){
    var idprovinsi = $(this).val();
    //console.log(idprovinsi);
    if (idprovinsi) {
      $.ajax({
        type:"GET",
        url:"{{ url('wilayah/kota') }}?idprovinsi="+idprovinsi,
        success: function(data){
          //console.log(data);
          $('#kota').empty();
          $("#kota").append('<option></option>');
          $.each(data,function(value,key){
             $("#kota").append('<option value="'+ key +'">'+ value +'</option>');
          });
        }
      });
    }else {
      $("#kota").empty();
    }
  })
  $('#kota').change(function() {
    var idkota = $(this).val();
    //console.log(idkota);
    if (idkota) {
      $.ajax({
        type:"GET",
        url:"{{ url('wilayah/kecamatan') }}?idkota="+idkota,
        success:function(data){
          $('#kecamatan').empty();
          $('#kecamatan').append('<option></option>');
          $.each(data,function(value,key){
            $('#kecamatan').append('<option value="'+key+'">'+value+'</option>');
          });
        }
      });
    }else {
      $('#kecamatan').empty();
    }
  });
  $('#kecamatan').change(function(event) {
    var idkecamtan = $(this).val();
    if (idkecamtan) {
      $.ajax({
        type:"GET",
        url:"{{ url('wilayah/kelurahan') }}?idkecamatan="+idkecamtan,
        success:function(data){
          $('#kelurahan').empty();
          $('#kelurahan').append('<option></option>');
          $.each(data,function(value,key){
            $('#kelurahan').append('<option value="'+key+'">'+value+'</option>');
          });
        }
      });
    }else {

    }
  });
});
</script>
@endsection
