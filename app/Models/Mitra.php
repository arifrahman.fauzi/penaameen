<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mitra extends Model
{
     protected $table = 'tb_mitra';
     protected $primaryKey = 'id_mitra';
     //public $timestamps = false;
}
