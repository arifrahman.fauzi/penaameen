<?php


  function checkPermission($permissions){

    $userAccess = getMyPermission(auth()->user()->role);

    foreach ($permissions as $key => $value) {

      if($value == $userAccess){

        return true;

      }

    }

    return false;

  }


  function getMyPermission($id)

  {

    switch ($id) {

      case 1:

        return 'gmf';

        break;

      case 2:

        return 'cabang';

        break;

      case 3:

        return 'mitra';

        break;

      case 4:

        return 'superadmin';

        break;

      default:

        return 'personal';

        break;

    }

  }


?>
