<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Mitra;
use Illuminate\Support\Facades\View;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = Auth::user()->nomor_anggota;
      $profil = Mitra::where('nomor_anggota',$user)->first();
      $mitra = DB::table('tb_mitra')->where('tb_mitra.nomor_anggota',$user)
              ->join('users','tb_mitra.nomor_anggota','=','users.nomor_anggota')
              ->join('tb_role','users.role','=','tb_role.id_role')
              ->first();
              $kota = DB::table('kabupaten')->get();
      return view('profile',['profil'=>$profil,'kota'=>$kota])->with('user',$mitra);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function biodata(Request $request, $id){
      dd($request,$id);
      $data = Mitra::find($id);
      $data->nama_mitra = $request->nama;

      $data->save();


      return response()->json(["message"=>"data berhasil di update"]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = Auth::user()->id;
      $data =  DB::table('users')
            ->where('id', $id)
            ->update(['name' => $request->nama,
            'username' => $request->username,
            'email' => $request->email,
          ]);
          return response()->json(["message"=>"success"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // protected function validator(array $data){
    //   return Validator::make($data, [
    //     'password'=>['required', 'string', 'min:6', 'confirmed'],
    //   ]);
    // }
    public function formreset(){
      return view('auth.passwords.reset');
    }
    public function resetpassword(Request $request){
      $id = Auth::user()->id;
      $password = Hash::make($request->password);
      DB::table('users')
              ->where('id', $id)
              ->update(['password' => $password]);
      return redirect('/home')->with('status', 'Profile updated!');
    }



    public function avatar(Request $request){
      $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4048',
        ]);

        $user = Auth::user();

        $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension(); // mengatur penamaan avatar

        $request->avatar->storeAs('avatar',$avatarName); // menyimpan avatar ke folder

        $user->avatar = $avatarName; // setelah nama di atur di masukkan ke database
        $user->save();

        return back()
            ->with('success','You have successfully upload image.');
    }
}
