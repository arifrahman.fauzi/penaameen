<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      if (Auth::user()->role == 4) {
        $idgmf = Auth::user()->nomor_gmf ;
        $nomorgmf = DB::table('tb_mitra')->where('id_mitra',$idgmf)->first();
        return view('admin.admin-dashboard')->with('gmf',$nomorgmf);
      }
      elseif (Auth::user()->role == 2) {
        return view('mitra.mitra-dashboard');
      }
      elseif (Auth::user()->role == 3) {
        return view('mitra.mitra-home');
      }
        return view('home');
    }
}
