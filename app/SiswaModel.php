<?php
#InventoryModel.php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SiswaModel extends Model
{
    protected $table = 'tb_siswa';

    protected $primaryKey = 'id';

    protected $fillable = [
    	'nama_siswa',
    	'jumlah',
    	'tempat_lahir',
    	'tanggal_lahir',
    	'alamat',
    	'alamat_sekolah',
      'kelas',
      'program_kursus',
      'hari_kursus',
      'jenis_kelas',
    ];

    //relasi many to one (Saya adalah anggota dari model ......)
    public function get_nama_ortu(){
        return $this->belongsTo('App\\Model\\OrtuModel', 'kategori', 'id');
    }

}
